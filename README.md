# Flaskr

The basic blog app built in the Flask tutorial.

## Install

Clone

```shell
$ git clone https://gitlab.com/KonstantinTogoi/flaskr
$ cd flaskr
```

Create a virtualenv and activate it:

```shell
$ python3 -m venv venv
$ . venv/bin/activate
```

Install Flaskr:

```shell
$ pip install -e .
```

## Run

```shell
$ export FLASK_APP=flaskr
$ export FLASK_ENV=development
$ flask init-db
$ flask run
```

Open https://127.0.0.1:5000 in a browser.

## Test

```shell
$ pip install '.[test]'
$ pytest
```

Run with coverage report:

```shell
$ coverage run -m pytest
$ coverage report
$ coverage html  # open htmlcov/index.html in a browser
```

